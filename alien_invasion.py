import pygame
import game_functions as gf

from settings import Settings
from game_stats import GameStats
from ship import Ship
from pygame.sprite import Group
from button import Button
from scoreboard import Scoreboard


def run_game():
# Initialize game and create a screen object.
	pygame.mixer.pre_init(44100, -16, 1, 512)
	pygame.init()
	blaster = pygame.mixer.Sound('music/blaster.wav')
	explosion = pygame.mixer.Sound('music/explosion.wav')
	pygame.mixer.music.load('music/background_music1.wav')
	ai_settings = Settings()
	screen = pygame.display.set_mode((ai_settings.screen_width, ai_settings.screen_height))
	pygame.mixer.music.play(-1)
	pygame.display.set_caption("Alien Invasion")
	# Make the Play button.
	play_button = Button(ai_settings, screen, "Play")
	#Create an instance to store game statistics and create a scoreboard.
	stats = GameStats(ai_settings)
	sb = Scoreboard(ai_settings, screen, stats)
	# Makd a ship, a group of bullets and group of aliens.
	ship = Ship(ai_settings, screen)
	bullets = Group()
	aliens = Group()
	#Create the fleet of aliens.
	gf.create_fleet(ai_settings, screen, ship, aliens)
	# Start the main loop for the game.
	while True:
		# Watch for keyboard and mouse events.
		#screen.fill(ai_settings.bg_color)
		gf.check_events(ai_settings, screen, stats, sb, play_button, ship, aliens, bullets, blaster)
		if stats.game_active:
			ship.update()
			gf.update_bullets(ai_settings, screen, stats, sb, ship, aliens, bullets, explosion)
			gf.update_aliens(ai_settings, stats, sb, screen, ship, aliens, bullets)
		gf.update_screen(ai_settings, screen, stats, sb, ship, aliens, bullets, play_button)
		

run_game()